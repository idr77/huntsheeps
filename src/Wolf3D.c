#include "Wolf3D.h"
#include <g3x.h>
#include <stdio.h>
#include "G3XTools.h"
#include "CameraHandler.h"

/**
 * @brief Angle of paws.
 */
static int anglePaws = 0;

/**
 * @brief Orientation of the wolf.
 */
static int orientation = 0;

/**
 * @brief Color of the wolf.
 */
static G3Xcolor colorBodyWolf = { 1.0, 0.0, 0.0 };

/**
 * @brief Indicates whether the wolf's coordinates are set.
 */
static bool set = false;

/**
 * @brief Coordinates of the wolf.
 */
static float xPos, yPos, xSpeed, ySpeed, radius;
static float xFace, yFace;
static float zPaw = 0.7, rPaw = 0.1;
static float zBody = 0.7 * 3, rBody = 0.7;
static float zNeck = 0.3, rNeck = 0.2;
static float rFace = 0.6;
static float zEar = 0.25, rEar = 0.2;


/**
 * @brief Draw the face of the wolf.
 */
static void Wolf3D_face(void);

/**
 * @brief Draw the ears of the wolf.
 */
static void Wolf3D_earLeft(void);
static void Wolf3D_earRight(void);

/**
 * @brief Draw the neck of the wolf.
 */
static void Wolf3D_neck(void);

/**
 * @brief Draw the wolf's body.
 */
static void Wolf3D_body(void);

/**
 * @brief Draw the wolf's paws.
 */
static void Wolf3D_pawUL(void);
static void Wolf3D_pawUR(void);
static void Wolf3D_pawDL(void);
static void Wolf3D_pawDR(void);

void Wolf3D_setParticleCoord(float posX, float posY, float speedX, float speedY,
		float radiusWolf) {
	xPos = posX;
	yPos = posY;
	xSpeed = speedX;
	ySpeed = speedY;
	if (radiusWolf > radius) {
		rBody += 0.05;
		zBody += 0.1;
		rNeck += 0.02;
		rFace += 0.05;
		rEar += 0.01;
		rPaw += 0.005;
		zPaw += 0.05;
	}
	xFace = posX - zBody / 2;
	yFace = posY;
	radius = radiusWolf;
	set = true;
}

int Wolf3D_isSet(void) {
	return set == true;
}

void Wolf3D_draw(void) {
	if (set) {
		Wolf3D_face();
		Wolf3D_earLeft();
		Wolf3D_earRight();
		Wolf3D_neck();
		Wolf3D_body();
		Wolf3D_pawUL();
		Wolf3D_pawUR();
		Wolf3D_pawDL();
		Wolf3D_pawDR();
	}
}

void Wolf3D_movePaws(void) {
	/*static bool isUp = true;
	isUp = (anglePaws > 30) ? false : (anglePaws < 330) ? true : isUp;
	anglePaws = (anglePaws == 0 && !isUp) ? 360 - anglePaws :
				(anglePaws == 360 && isUp) ? 0 : anglePaws;
	anglePaws += isUp ? 3 : -3;
	*/
}

int Wolf3D_getOrientation(void) {
	if (xSpeed == 0 && ySpeed > 0) {
		orientation = 90;
	} else if (xSpeed == 0 && ySpeed < 0) {
		orientation = 270;
	} else if (xSpeed < 0) {
		orientation = modulo(rad2Deg(atanf(ySpeed / xSpeed)) + 180, 360);
	} else if (xSpeed > 0 && ySpeed < 0) {
		orientation = modulo(rad2Deg(atanf(ySpeed / xSpeed)) + 360, 360);
	} else {
		orientation = rad2Deg(atanf(ySpeed / xSpeed));
	}
	return orientation;
}

static void Wolf3D_face(void) {
	pushMatrix();
	makeTranslation(xFace, yFace, zPaw + rBody + zNeck);
	makeRotation(270, Y_AXIS);
	makeRotation(orientation, Z_AXIS);
	renderSphere(colorBodyWolf, rFace);
	makeTranslation(rFace - rFace/3, 0, 0);
	popMatrix();
}

static void Wolf3D_earLeft(void) {
	pushMatrix();
	makeTranslation(xFace, yFace - rFace / 2, zPaw + rBody + zNeck + rFace - rFace/8);
	renderCone(colorBodyWolf, rEar, zEar);
	popMatrix();
}

static void Wolf3D_earRight(void) {
	pushMatrix();
	makeTranslation(xFace, yFace + rFace / 2, zPaw + rBody + zNeck + rFace - rFace/8);
	renderCone(colorBodyWolf, rEar, zEar);
	popMatrix();
}

static void Wolf3D_neck(void) {
	pushMatrix();
	makeTranslation(xFace, yPos, zPaw + rBody);
	renderPerfectCylinder(colorBodyWolf, rNeck, zNeck);
	popMatrix();
}

static void Wolf3D_body(void) {
	pushMatrix();
	makeTranslation(xPos, yPos, zPaw);
	makeRotation(270, Y_AXIS);
	makeTranslation(0, 0 , -zBody/2);
	makeRotation(orientation, Z_AXIS);
	renderPerfectCylinder(colorBodyWolf, rBody, zBody);
	popMatrix();
}

static void Wolf3D_pawUL(void) {
	pushMatrix();
	makeTranslation(xPos - zBody + zBody / 1.5, yPos - rBody / 2, 0.0);
	makeRotation(anglePaws, Y_AXIS);
	renderPerfectCylinder(colorBodyWolf, rPaw, zPaw);
	popMatrix();
}

static void Wolf3D_pawUR(void) {
	pushMatrix();
	makeTranslation(xPos - zBody + zBody / 1.5, yPos + rBody / 2, 0.);
	makeRotation(anglePaws, Y_AXIS);
	renderPerfectCylinder(colorBodyWolf, rPaw, zPaw);
	popMatrix();
}

static void Wolf3D_pawDL(void) {
	pushMatrix();
	makeTranslation(xPos + zBody - zBody / 1.5, yPos - rBody / 2, 0.);
	makeRotation(anglePaws, Y_AXIS);
	renderPerfectCylinder(colorBodyWolf, rPaw, zPaw);
	popMatrix();
}

static void Wolf3D_pawDR(void) {
	pushMatrix();
	makeTranslation(xPos + zBody - zBody / 1.5, yFace + rBody / 2, 0.);
	makeRotation(anglePaws, Y_AXIS);
	renderPerfectCylinder(colorBodyWolf, rPaw, zPaw);
	popMatrix();
}

void Wolf3D_view(void) {
	G3Xpoint p = {0., 0., 0.};
	g3x_SetPerspective(40, 40, 2.5);
	Camera_setTarget(p);
	Wolf3D_setParticleCoord(0., 0., 0., 0., 1.);
	Wolf3D_draw();
	Wolf3D_movePaws();

}
