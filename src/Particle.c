#include "Particle.h"

/**
 * @brief Constant Epsilon used for rounding errors.
 */
#define EPSILON 1.E-6

/** 
 * @brief Normalizes a vector and limits rounding errors.
 */
static double Normalize(G2Xvector *v, double newN);

/**
 * @brief Replaces a particle which is in an obstacle.
 */
static void Particle_partInside(Particle *p, Obstacle *o);


void Particle_create(Particle *p, int xMin, int xMax, int yMin, int yMax, G2Xpoint pos, G2Xvector speed, double radius, double color[]) {
	int i;
	p->xMin = 0.95*xMin;
	p->xMax = 0.95*xMax;
	p->yMin = 0.95*yMin;
	p->yMax = 0.95*yMax;
	p->pos = pos;
	p->speed = speed;
	for (i = 0; i < 3 ; i++)  
		p->color[i] = color[i];
	p->radius = radius;
	p->next = NULL;
	p->draw = Particle_drawSpot;
	p->edge = Particle_rebound;
	p->move = Particle_move;
	p->alive = true;
}

void Particle_createRandom(Particle *p, int xMin, int xMax, int yMin, int yMax) {
	int i;
	p->xMin = 0.95 * xMin;
	p->xMax = 0.95 * xMax;
	p->yMin = 0.95 * yMin;
	p->yMax = 0.95 * yMax;
	p->pos = g2x_Point(g2x_RandDelta(0, p->xMax), g2x_RandDelta(0, p->yMax));
	p->speed = g2x_Vector_XY(g2x_RandDelta(0, 5.0), g2x_RandDelta(0, 5.));
	for (i = 0; i < 3 ; i++)  
		p->color[i] = g2x_RandDelta(0.5, 0.5);
	p->radius = g2x_RandDelta(10, 5.0);
	p->next = NULL;
	p->draw = Particle_drawSpot;
	p->edge = Particle_rebound;
	p->move = Particle_move;
	p->alive = true;
}

void Particle_drawSpot(Particle *p) {
	g2x_Spot(p->pos.x, p->pos.y, p->radius, p->color);
}

void Particle_drawCircle(Particle *p) {
	g2x_Circle(p->pos.x, p->pos.y, p->radius, p->color, 1);
}

void Particle_rebound(Particle *p) {
	double s = Normalize(&p->speed, 1.0);
	p->speed.x *= (p->pos.x <= 0.98*p->xMin || p->pos.x >= 0.98*p->xMax)? -1 : 1;
	p->speed.y *= (p->pos.y <= 0.98*p->yMin || p->pos.y >= 0.98*p->yMax)? -1 : 1;
	Normalize(&p->speed, s);
}

void Particle_teleport(Particle *p) {
	p->pos.x += (p->pos.x < 0.99*p->xMin)? p->xMax - p->xMin : (p->pos.x > 0.99*p->xMax)? -(p->xMax - p->xMin) : 0;
	p->pos.y += (p->pos.y < 0.99*p->yMin)? p->yMax - p->yMin : (p->pos.y > 0.99*p->yMax)? -(p->yMax - p->yMin) : 0;
}

void Particle_move(Particle *p) {
	float x = p->pos.x + p->speed.x;
	float y = p->pos.y + p->speed.y;
	if (x > p->xMin && x < p->xMax && y > p->yMin && y < p->yMax) {
		p->pos.x = x;
		p->pos.y = y;
	}
}

void Particle_setDrawFunction(Particle *p, ParticleFunc drawMethod){
	p->draw = drawMethod;
}

void Particle_setEdgeFunction(Particle *p, ParticleFunc edgeMethod) {
	p->edge = edgeMethod;
}

void Particle_follow(Particle *follower, Particle *toFollow, double ratio) {
	G2Xvector ftF = g2x_Vector(follower->pos, toFollow->pos);
	Normalize(&ftF, 1.0);
	double normSpeed = Normalize(&follower->speed, 1.0);
	follower->speed.x = (1 - ratio) * follower->speed.x + ratio * ftF.x;
	follower->speed.y = (1 - ratio) * follower->speed.y + ratio * ftF.y;
	Normalize(&follower->speed, normSpeed);
}

void Particle_escape(Particle *runaway, Particle *toEscape, double ratio) {
	G2Xvector ftEsc = g2x_Vector(runaway->pos, toEscape->pos);
	Normalize(&ftEsc, 1.0);
	double normSpeed = Normalize(&toEscape->speed, 1.0);
	runaway->speed.x = ratio * runaway->speed.x + (1-ratio) * ftEsc.x;
	runaway->speed.y = ratio * runaway->speed.y + (1-ratio) * ftEsc.y;
	Normalize(&runaway->speed, normSpeed);
}

void Particle_print(Particle *p) {
	printf("Pos(x,y) = (%f, %f)\n", p->pos.x, p->pos.y);
	printf("Speed(x,y) = (%f, %f)\n", p->speed.x, p->speed.y);
	printf("Color = (%f , %f, %f)\n", p->color[0], p->color[1], p->color[2]);
	printf("Radius = %f\n", p->radius);
}

bool Particle_hasCollision(const Particle *p, const Obstacle *circle, G2Xvector *dir) {
	G2Xvector PC = g2x_Vector(p->pos, circle->center);
	double d2 = G2Xsqrnorm(PC);
	double r2 = SQR(circle->radius);

	if (d2 > 3 * r2) {
		return false;
	}

	if (G2Xprodscal(p->speed, PC) < 0) {
		return false;
	}

	double I = r2 / d2;
	double J = 1 - I;
	double s = sqrt(I * J);
	G2Xvector vPI, vPJ;

	vPI.x = J * PC.x - s * PC.y;
	vPI.y = J * PC.y + s * PC.x;
	vPJ.x = J * PC.x + s * PC.y;
	vPJ.y = J * PC.y - s * PC.x;

	I = G2Xprodvect(p->speed, vPI);
	J = G2Xprodvect(p->speed, vPJ);
	if (I * J == 0) {
		return false;
	}

	*dir = fabs(I) < fabs(J) ? vPI : vPJ;
	return true;
}


void Particle_deviate(Particle *p, const Obstacle *circle, G2Xvector *dir) {
	double normSpeed = Normalize(&p->speed, 1);
	Normalize(dir, 1.);
	double dist = g2x_Distance(p->pos, circle->center);
	double nRatio =  pow((3*circle->radius - dist)/(2*circle->radius), 3); /* Optimize ratio with distance between object and obstacle */
	p->speed.x = (1 - nRatio) * p->speed.x + nRatio * dir->x;
	p->speed.y = (1 - nRatio) * p->speed.y + nRatio * dir->y;
	Normalize(&p->speed, normSpeed);
}

void Particle_avoidAllObstacles(Particle *p, Obstacle *obstacles, int nbObstacles) {
	Obstacle *o = obstacles, *coll = NULL;
	double d2, r2, a, b, s, z = 10000. ;
	G2Xvector pI, pJ, pC, dir;

	for (o = obstacles; o < obstacles + nbObstacles ; o++) {
		pC = g2x_Vector(p->pos, o->center); /* PC> */
		d2 = G2Xsqrnorm(pC); /* PC^2 */
		r2 = SQR(o->radius); /* r^2 */

		if (d2 < r2) {
			Particle_partInside(p, o); /* Particule dans l'obstacle */
			return;
		}
		if (d2 < 9 * r2) {
			continue;	/* La particule est trop loin */
		}
		if (G2Xprodscal(p->speed, pC) <= 0.) {
			continue;	/* La particule est dans la mauvaise direction */
		}
		if (coll != NULL && d2 > z) {
			continue;
		}

		/* Calcul du cone de collision */
		a = r2 / d2;
		b = 1 - a;
		s = sqrt(a * b);
		pI.x = b * pC.x - s * pC.y;
		pI.y = b * pC.y + s * pC.x;
		pJ.x = b * pC.x + s * pC.y;
		pJ.y = b * pC.y - s * pC.x;
		a = G2Xprodvect(p->speed, pI);
		b = G2Xprodvect(p->speed, pJ);

		if (a * b >= 0.) {
			continue;	/* Hors du cone de collision */
		}

		/* Risque de collision */
		coll = o;
		z = d2;
		dir = a*a < b*b ? pI : pJ;
	}
	if (coll) {
		Particle_deviate(p, coll, &dir);
	}
}

void Particle_followTarget(Particle *p, G2Xpoint M, double alpha) {
	G2Xvector pM = g2x_Vector(p->pos, M);
	Normalize(&pM, 1.);
	double speed = Normalize(&p->speed, 1.0);
	p->speed.x = (1 - alpha) * p->speed.x + alpha * pM.x;
	p->speed.y = (1 - alpha) * p->speed.y + alpha * pM.y;
	Normalize(&p->speed, speed);
}

void Particle_avoidCollision(Particle *p, Particle *other) {
	G2Xvector dir = g2x_Vector(p->pos, other->pos);
	double s0, s1;
	double d = Normalize(&dir, 1.);
	double vp = Normalize(&p->speed, 1.0);
	double vOther = Normalize(&other->speed, 1.0);
	double v = vp + vOther;
	s1 = 2.5 * v;
	s0 = 0.5 * v;
	if (!(d > s1)) { 
		double ratio = pow((s1 - d) / (s1 - s0), 3.);
		p->speed.x = (1 - ratio) * p->speed.x - ratio * dir.x;
		p->speed.y = (1 - ratio) * p->speed.y - ratio * dir.y;
		other->speed.x = (1 - ratio) * other->speed.x + ratio * dir.x;
		other->speed.y = (1 - ratio) * other->speed.y + ratio * dir.y;
		Normalize(&p->speed, vp);
		Normalize(&other->speed, vOther);
	}
}

void Particle_avoidCollisions(Particle *particles, int nbParticles) {
	int i , j;
	for (i = 0; i < nbParticles - 1; i++) {
		for (j = i+1; j < nbParticles ; j++) {
			Particle_avoidCollision(particles+i, particles+j);
		}
	}
}

void Particle_hunt(Particle *hunter, Particle *hunteds , int nbHunteds, double ratio) {
	G2Xvector Lm;
	G2Xvector buf = {0. , 0.};
	double d = 0, Vm;
	double s0 = 2*hunter->radius, s1 = 19*hunter->radius;
	Particle *h;
	for (h = hunteds; h < hunteds + nbHunteds ; h++) {
		Lm = g2x_Vector(hunter->pos, h->pos);
		d = Normalize(&Lm, 1.);
		Vm = Normalize(&h->speed, 1.);
		if (!h->alive || d > s1) {
			continue;
		}
		if (d < s0) {
			hunter->radius *= 1.02;
			h->alive = false;
			return;
		}
		d = 1 / pow(d, 3.);
		buf.x += d * Lm.x;
		buf.y += d * Lm.y;
		Normalize(&buf, 1.0);

		/* Current hunted particle escape the hunter */
		double a = pow((s1 - d)/(s1 - s0), 4.0);

		h->speed.x = (1 - a) * h->speed.x + a * Lm.x;
		h->speed.y = (1 - a) * h->speed.y + a * Lm.y;
		Normalize(&h->speed, Vm);
	}
	double sp = Normalize(&hunter->speed, 1.0);
	Normalize(&buf, 1.);
	hunter->speed.x = (1 - ratio) * hunter->speed.x + 1.5 * ratio * buf.x;
	hunter->speed.y = (1 - ratio) * hunter->speed.y + 1.5 * ratio * buf.y;
	Normalize(&hunter->speed, sp);
}

int Particle_getNbElements(Particle *particles) {
	if (!particles) {
		return 0;
	}
	if (particles->alive) {
		return 1 + Particle_getNbElements(particles+1);
	}
	return Particle_getNbElements(particles);
}

static double Normalize(G2Xvector *v, double newN) {
	double oldN = g2x_Norme(*v);
	if (oldN < EPSILON) {
		oldN = EPSILON;
	}
	newN /= oldN;
	v->x *= newN;
	v->y *= newN;
	return oldN;
}

static void Particle_partInside(Particle *p, Obstacle *o) {
	G2Xvector pC = g2x_Vector(p->pos, o->center);
	Normalize(&pC, 1.);

	/* On replace la particule à la surface de l'obstacle */
	p->pos.x = o->center.x - o->radius * pC.x;
	p->pos.y = o->center.y - o->radius * pC.y;

	/* Vitesse : Tangente au cercle obstacle */
	double speed = Normalize(&p->speed, 1.);
	int s = G2Xprodvect(p->speed, pC) > 0 ? 1 : -1;
	p->speed.x = s * pC.y;
	p->speed.y = -s * pC.x;
	Normalize(&p->speed, speed);
}
