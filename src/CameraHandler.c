#include "CameraHandler.h"
#include <g3x.h>

static double dist = 10., theta = 45., phi = 90.;

void Camera_setTarget(G3Xpoint t) {
	g3x_SetCameraSpheric(theta, phi, dist, t);
}

void Camera_actionZoomIn(void) {
	dist -= 2;
	g3x_SetCameraDist(dist);
}

void Camera_actionZoomOut(void) {
	dist -= 2;
	g3x_SetCameraDist(dist);
}

void Camera_actionLeft(void) {
	theta -= 0.2;
	g3x_SetCameraTheta(theta);
}

void Camera_actionRight(void) {
	theta += 0.2;
	g3x_SetCameraTheta(theta);
}

void Camera_actionUp(void) {
	phi -= 0.2;
	g3x_SetCameraPhi(phi);
}

void Camera_actionDown(void) {
	phi += .2;
	g3x_SetCameraPhi(phi);
}
