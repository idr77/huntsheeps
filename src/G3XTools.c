#include "G3XTools.h"

/**
 * @brief Structure which defines the light.
 */
typedef struct _Light {
	float ambiance;
	float diffuse;
	float specularity;
	float shine;
	float transparency;
} Light;

/**
 * @brief Current light.
 */
static Light light;

/**
 * @brief Quadric object used to draw objects.
 */
static GLUquadric *quadric;

/**
 * @brief Indicates whether the quadric is allocated or not.
 */
static bool quadricInit;

/**
 * @brief Allocates the quadric.
 */
static void initQuadric(void);

/**
 * @brief Rendering parameters (color and light).
 */
static void materializeObject(G3Xcolor color);

int modulo(int a, int n) {
	return a >= 0 ? a % n : (a % n) + n;
}

int rad2Deg(float rad) {
	return 180 * rad / PI;
}

void Light_setParameters(float ambiance, float diffuse, float specularity,
		float shine, float transparency) {
	light.ambiance = ambiance;
	light.diffuse = diffuse;
	light.specularity = specularity;
	light.shine = shine;
	light.transparency = transparency;
}

void Light_setDefault(void) {
	Light_setParameters(0.2, 0.3, 0.4, 0.5, 0.5);
}

void Light_setAmbiance(float ambiance) {
	light.ambiance = ambiance;
}

void Light_setTransparency(float transparency) {
	light.transparency = transparency;
}

void Light_setDiffusion(float diffusion) {
	light.diffuse = diffusion;
}

void Light_setSpecularity(float spec) {
	light.specularity = spec;
}

void Light_setShine(float shine) {
	light.shine = shine;
}

void createG3Xpoint(const G2Xpoint *pt, G3Xpoint p) {
	p[0] = pt->x;
	p[1] = pt->y;
	p[2] = 0.0;
}

void pushMatrix() {
	glPushMatrix();
}

void popMatrix() {
	glPopMatrix();
}

void makeTranslation(float x, float y, float z) {
	glTranslatef(x, y, z);
}

void makeRotation(float angle, int axis) {
	switch (axis) {
	case X_AXIS:
		glRotatef(angle, 1., 0., 0.);
		break;
	case Y_AXIS:
		glRotatef(angle, 0., 1., 0.);
		break;
	case Z_AXIS:
		glRotatef(angle, 0., 0., 1.);
		break;
	default:
		return;
	}
}

void makeScale() {
	glScalef(0.5, 0.5, 0.5);
}

void renderSphere(G3Xcolor color, float size) {
	materializeObject(color);
	glutSolidSphere(size, 40, 40);
}

void renderCone(G3Xcolor color, float size, float height) {
	materializeObject(color);
	initQuadric();
	glutSolidCone(size, height, 30, 30);
	gluQuadricOrientation(quadric, GLU_INSIDE);
	gluDisk(quadric, 0.0, size, 30, 30);
}

void renderCylinder(G3Xcolor color, float radiusDown, float radiusUp,
		float height) {
	materializeObject(color);
	initQuadric();
	glPushMatrix();
	gluQuadricOrientation(quadric, GLU_OUTSIDE);
	gluCylinder(quadric, radiusDown, radiusUp, height, 30, 30);

	gluQuadricOrientation(quadric, GLU_INSIDE);
	gluDisk(quadric, 0.0, radiusDown , 30, 30);

	glTranslatef(0, 0, height);
	gluQuadricOrientation(quadric, GLU_OUTSIDE);
	gluDisk(quadric, 0.0, radiusUp, 30, 30);
	glPopMatrix();
}

void renderPerfectCylinder(G3Xcolor color, float radius, float height) {
	renderCylinder(color, radius, radius, height);
}

void renderCube(G3Xcolor color, float size) {
	materializeObject(color);
	glutSolidCube(size);
}

void renderRectangle(G3Xcolor color, float xmin, float xmax, float ymin,
		float ymax) {
	materializeObject(color);
	glRectf(xmin, ymin, xmax, ymax);
}

void renderHemisphere(G3Xcolor color, float radius) {
	int x, y, xMax = 24, yMax = 24;
	float vertices[xMax * yMax][3];

	/* Operates vertices of the sphere. */
	for (x = 0; x < xMax; x++) {
		float x0 = x * PI/(2*xMax);
		for (y = 0; y < yMax; y++) {
			float y0 = 2 * PI * y / yMax;
			vertices[x * yMax + y][0] = radius * cos(y0) * cos(x0);
			vertices[x * yMax + y][1] = radius * sin(x0);
			vertices[x * yMax + y][2] = radius * sin(y0) * cos(x0);
		}
	}

	materializeObject(color);

	/* Draws the sphere. */
	glBegin(GL_QUADS);
	for (x = 0; x < xMax-1; x++) {
		for (y = 0; y < yMax; y++) {
			int s1 = x * yMax + y;
			int s2 = x * yMax + (y+1) % yMax;
			int s3 = (x+1) * yMax + (y+1) % yMax;
			int s4 = (x+1) * yMax + y;

			glNormal3fv(vertices[s1]);
			glVertex3fv(vertices[s1]);

			glNormal3fv(vertices[s2]);
			glVertex3fv(vertices[s2]);

			glNormal3fv(vertices[s3]);
			glVertex3fv(vertices[s3]);

			glNormal3fv(vertices[s4]);
			glVertex3fv(vertices[s4]);
		}
	}
	glEnd();

	gluQuadricOrientation(quadric, GLU_OUTSIDE);
	gluDisk(quadric, 0.0, radius, xMax, yMax);
}

void releaseResources(void) {
	if (quadricInit) {
		gluDeleteQuadric(quadric);
		quadricInit = false;
	}
}

static void initQuadric(void) {
	if (!quadricInit) {
		quadric = gluNewQuadric();
		quadricInit = true;
	}
}

static void materializeObject(G3Xcolor color) {
	g3x_Material(color, light.ambiance, light.diffuse, light.specularity,
			light.shine, light.transparency);
}
