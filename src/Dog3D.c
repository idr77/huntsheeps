#include "Dog3D.h"
#include <g3x.h>
#include <stdio.h>
#include "G3XTools.h"
#include "CameraHandler.h"

/**
 * @brief Angle of paws.
 */
static int anglePaws = 0;

/**
 * @brief Orientation of the dog.
 */
static int orientation = 0;

/**
 * @brief Color of the dog.
 */
static G3Xcolor colorBodyDog = { 1.0, 1.0, 1.0 };

/**
 * @brief Indicates whether the dog's position is set.
 */
static bool set = false;

/**
 * @brief Coordinates of each part of the dog.
 */
static float xPos, yPos, xSpeed, ySpeed;
static float xFace, yFace;
static const float zPaw = 0.5, rPaw = 0.1;
static const float zBody = 0.6 * 3, rBody = 0.55;
static const float zNeck = 0.3, rNeck = 0.2;
static const float rFace = 0.6;
static const float zEar = 0.25, rEar = 0.2;

/**
 * @brief Draw the face of the dog.
 */
static void Dog3D_face(void);

/**
 * @brief Draw the ears of the dog.
 */
static void Dog3D_earLeft(void);
static void Dog3D_earRight(void);

/**
 * @brief Draw the neck of the dog.
 */
static void Dog3D_neck(void);

/**
 * @brief Draw the body of the dog.
 */
static void Dog3D_body(void);

/**
 * @brief Draw the dog's paws.
 */
static void Dog3D_pawUL(void);
static void Dog3D_pawUR(void);
static void Dog3D_pawDL(void);
static void Dog3D_pawDR(void);

void Dog3D_setParticleCoord(float posX, float posY, float speedX, float speedY) {
	xPos = posX;
	yPos = posY;
	xSpeed = speedX;
	ySpeed = speedY;
	xFace = posX - zBody / 2;
	yFace = posY;
	set = true;
}

int Dog3D_isSet(void) {
	return set == true;
}

void Dog3D_draw(void) {
	if (set) {
		Dog3D_face();
		Dog3D_earLeft();
		Dog3D_earRight();
		Dog3D_neck();
		Dog3D_body();
		Dog3D_pawUL();
		Dog3D_pawUR();
		Dog3D_pawDL();
		Dog3D_pawDR();
	}
}

void Dog3D_movePaws(void) {
	/*static bool isUp = true;
	isUp = (anglePaws > 30) ? false : (anglePaws < 330) ? true : isUp;
	anglePaws = (anglePaws == 0 && !isUp) ? 360 - anglePaws :
				(anglePaws == 360 && isUp) ? 0 : anglePaws;
	anglePaws += isUp ? 1 : -1;*/
}

int Dog3D_getOrientation(void) {
	if (xSpeed == 0 && ySpeed > 0) {
		orientation = 90;
	} else if (xSpeed == 0 && ySpeed < 0) {
		orientation = 270;
	} else if (xSpeed < 0) {
		orientation = modulo(rad2Deg(atanf(ySpeed / xSpeed)) + 180, 360);
	} else if (xSpeed > 0 && ySpeed < 0) {
		orientation = modulo(rad2Deg(atanf(ySpeed / xSpeed)) + 360, 360);
	} else {
		orientation = rad2Deg(atanf(ySpeed / xSpeed));
	}
	return orientation;
}

static void Dog3D_face(void) {
	pushMatrix();
	makeTranslation(xFace, yFace, zPaw + rBody + zNeck);
	makeRotation(270, Y_AXIS);
	makeRotation(orientation, Z_AXIS);
	renderSphere(colorBodyDog, rFace);
	makeTranslation(rFace - rFace/3, 0, 0);
	popMatrix();
}

static void Dog3D_earLeft(void) {
	pushMatrix();
	makeTranslation(xFace, yFace - rFace / 2, zPaw + rBody + zNeck + rFace - rFace/8);
	renderCone(colorBodyDog, rEar, zEar);
	popMatrix();
}

static void Dog3D_earRight(void) {
	pushMatrix();
	makeTranslation(xFace, yFace + rFace / 2, zPaw + rBody + zNeck + rFace - rFace/8);
	renderCone(colorBodyDog, rEar, zEar);
	popMatrix();
}

static void Dog3D_neck(void) {
	pushMatrix();
	makeTranslation(xFace, yPos, zPaw + rBody);
	renderPerfectCylinder(colorBodyDog, rNeck, zNeck);
	popMatrix();
}

static void Dog3D_body(void) {
	pushMatrix();
	makeTranslation(xPos, yPos, zPaw);
	makeRotation(270, Y_AXIS);
	makeTranslation(0, 0 , -zBody/2);
	makeRotation(orientation, Z_AXIS);
	renderPerfectCylinder(colorBodyDog, rBody, zBody);
	popMatrix();
}

static void Dog3D_pawUL(void) {
	pushMatrix();
	makeTranslation(xPos - zBody + zBody / 1.5, yPos - rBody / 2, 0.0);
	makeRotation(anglePaws, Y_AXIS);
	renderPerfectCylinder(colorBodyDog, rPaw, zPaw);
	popMatrix();
}

static void Dog3D_pawUR(void) {
	pushMatrix();
	makeTranslation(xPos - zBody + zBody / 1.5, yPos + rBody / 2, 0.);
	makeRotation(anglePaws, Y_AXIS);
	renderPerfectCylinder(colorBodyDog, rPaw, zPaw);
	popMatrix();
}

static void Dog3D_pawDL(void) {
	pushMatrix();
	makeTranslation(xPos + zBody - zBody / 1.5, yPos - rBody / 2, 0.);
	makeRotation(anglePaws, Y_AXIS);
	renderPerfectCylinder(colorBodyDog, rPaw, zPaw);
	popMatrix();
}

static void Dog3D_pawDR(void) {
	pushMatrix();
	makeTranslation(xPos + zBody - zBody / 1.5, yFace + rBody / 2, 0.);
	makeRotation(anglePaws, Y_AXIS);
	renderPerfectCylinder(colorBodyDog, rPaw, zPaw);
	popMatrix();
}

void Dog3D_view(void) {
	G3Xpoint p = {0., 0., 0.};
	g3x_SetPerspective(40, 40, 2.5);
	Camera_setTarget(p);
	Dog3D_setParticleCoord(0., 0., 0., 0.);
	Dog3D_draw();
	Dog3D_movePaws();

}
