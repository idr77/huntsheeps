#include "G3XTools.h"
#include "GUI3D.h"
#include "Dog3D.h"
#include "Wolf3D.h"
#include "Sheep3D.h"
#include "CameraHandler.h"

/**
 * @brief  Min and max coordinates. 
 */
double xmin = -200., ymin = -100., xmax = +200., ymax = +100.;

/**
 * @brief Boolean used to know whether the initialization is done or not.
 */
static bool initDone = false;

void initCamera(G3Xpoint *camera, G3Xpoint *tracker) {
	/* Camera */
	g3x_SetPerspective(50, 100., 2.5);
	g3x_SetCameraTracking(camera, tracker);
	g3x_SetKeyAction('z', Camera_actionZoomIn, "Zoom in");
	g3x_SetKeyAction('s', Camera_actionZoomOut, "Zoom out");
	g3x_SetKeyAction('q', Camera_actionLeft, "Pan left");
	g3x_SetKeyAction('d', Camera_actionRight, "Pan right");
	g3x_SetKeyAction('a', Camera_actionUp, "Pan up");
	g3x_SetKeyAction('e', Camera_actionDown, "Pan down");

	/* Light */
	g3x_SetLightAmbient(1., 1., 1.);
	g3x_SetLightDiffuse(1., 1., 1.);
	g3x_SetLightSpecular(1., 1., 1.);
	g3x_SetLightPosition(10., 10., 10.);
	g3x_SetLightDirection(0., 0., 0.);
	Light_setDefault();

	/* Refresh */
	/*g3x_SetRefreshFreq(2);*/

	initDone = true;
	printf("Initialization is done !\n");
}

int isInitialized() {
	return initDone == true;
}

void drawGround() {
	G3Xcolor colorGround = { 0.0, 0.4, 0.1 };
	renderRectangle(colorGround, xmin, xmax, ymin, ymax);
}

void drawDog(Particle *p) {
	Dog3D_setParticleCoord(p->pos.x, p->pos.y, p->speed.x, p->speed.y);
	Dog3D_draw();
}

void drawSheep(Particle *p) {
	Sheep3D_draw(p);
}

void drawWolf(Particle *p) {
	Wolf3D_setParticleCoord(p->pos.x, p->pos.y, p->speed.x, p->speed.y, p->radius);
	Wolf3D_draw();
}

void drawRock(Obstacle *o) {
	G3Xcolor colorRock = { 0.9, 0.9, 0.9 };
	pushMatrix();
	makeTranslation(o->center.x, o->center.y, 0);
	renderSphere(colorRock, o->radius / 2);
	popMatrix();
}

void drawTree(Obstacle *o) {
	int i;
	if (o->radius > 2.0) {
		o->radius = 2.0;
	}
	G3Xcolor cRoot = { 0.7, 0.3, 0.1 };
	G3Xcolor cLeaves = { 0.0, 0.6, 0.2 };
	pushMatrix();
	makeTranslation(o->center.x, o->center.y, 0);
	renderPerfectCylinder(cRoot, 0.7, 3.0);
	for (i = 0; i < 3; i++) {
		makeTranslation(0., 0., 2.5);
		renderCone(cLeaves, 1.5, 3.0);
	}
	popMatrix();
}
