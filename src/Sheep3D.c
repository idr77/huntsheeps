#include "Sheep3D.h"
#include <g3x.h>
#include "G3XTools.h"

void Sheep3D_draw(Particle *sheep) {
	G3Xcolor colorHead = { 0.0, 0.0, 0.0 };
	G3Xcolor colorSheep = { 1.0, 1.0, 1.0 };
	pushMatrix();
	makeTranslation(sheep->pos.x, sheep->pos.y, 0.5);
	renderSphere(colorSheep, sheep->radius);
	makeTranslation(-sheep->radius, 0, 0);
	renderSphere(colorHead, sheep->radius/50);
	popMatrix();
}
