#include <g3x.h>
#include <g2x.h>
#include "G3XTools.h"
#include "GUI3D.h"
#include "Particle.h"
#include "Obstacle.h"
#include "Dog3D.h"
#include "Wolf3D.h"

/* =======================================================================
 *                        Animation variables                            =
 * =======================================================================*/

/**
 * @brief Window dimensions (in pixels)  
 */
int width = 1200, height = 600;

/**
 * @brief Real coordinates of the window.
 */
extern double xmin, ymin, xmax, ymax;

/**
 * @brief Drawing Mode (true: 2D, false: 3D)
 */
static bool draw2D;

/**
 * @brief Number of sheeps.
 */
#define NB_SHEEPS 30

/**
 * @brief Number of obstacles.
 */
#define NB_OBSTACLES 19

/**
 * @brief Particle which defines a camera with its position.
 */
static Particle camera;
static G3Xpoint posCamera;

/**
 * @brief Particle which defines the camera tracker with its position.
 */
static Particle tracker;
static G3Xpoint posTracker;

/**
 * @brief Particle which defines the hunter (the wolf).
 */
static Particle hunter;

/**
 * @brief Particle which defines the dog (leader of sheeps).
 */
static Particle dog;

/**
 * @brief Sheep's particles.
 */
static Particle sheeps[NB_SHEEPS];

/**
 * @brief Obstacle's particles.
 */
static Obstacle obstacles[NB_OBSTACLES];

/**
 * @brief Boolean which informs whether the initialization is done or not.
 */
static bool initialized = false;

/**
 * @brief Checks whether a position is in the window.
 */
static bool PointInWindow(G2Xpoint M);

/**
 * @brief Initializes the hunter (the wolf).
 */
static void initHunter(void);

/**
 * @brief Initializes the camera and its tracker.
 */
static void initCameraTracker(void);

/**
 * @brief Initializes the obstacles.
 */
static void initObstacles(void);

/**
 * @brief Initializes the dog.
 */
static void initDog(void);

/**
 * @brief Initializes the sheeps.
 */
static void initSheeps(void);

/**
 * @brief Updates the position of the camera.
 */
static void updateCameraPosition(void);

/**
 * @brief 2D-rendering function.
 */
static void draw_2D(void);

/**
 * @brief 3D-rendering function.
 */
static void draw_3D(void);

/**
 * @brief Init function.
 */
static void Init(void);

/**
 * @brief Anim function.
 */
static void Anim(void);

/**
 * @brief Rendering function.
 */
static void Draw(void);

/**
 * @brief Exit function.
 */
static void Exit(void);

/*=============================================================================*/
/*= Main function                                                             =*/
/*=============================================================================*/
int main(int argc, char **argv) {
	draw2D = argc == 1 ? true : false;

	if (draw2D) {
		g2x_InitWindow(*argv, width, height);
		g2x_SetBkgCol(0.);
		g2x_SetWindowCoord(xmin, ymin, xmax, ymax);
		g2x_SetInitFunction(Init);
		g2x_SetDrawFunction(Draw);
		g2x_SetAnimFunction(Anim);
		g2x_SetExitFunction(Exit);
		return g2x_MainStart();
	} else {
		glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_MULTISAMPLE);
		g3x_InitWindow(*argv, width, height);
		g3x_SetBkgCol(0.7);
		g2x_SetWindowCoord(xmin, ymin, xmax, ymax);
		g3x_SetInitFunction(Init);
		g3x_SetExitFunction(Exit);
		g3x_SetDrawFunction(Draw);
		g3x_SetAnimFunction(Anim);
		Init();
		updateCameraPosition();
		initCamera(&posCamera, &posTracker);
		return g3x_MainStart();
	}
}

/*=============================================================================*/
/*= Initialization                                                            =*/
/*=============================================================================*/
static void Init(void) {
	if (!initialized) {
		initObstacles();
		initHunter();
		if (!draw2D) {
			initCameraTracker();
		}
		initDog();
		initSheeps();
		initialized = true;
	}
}

/*=============================================================================*/
/*= Animator function : Movements operations                                  =*/
/*=============================================================================*/
static void Anim(void) {
	Particle *P;
	/* Dog follows the mouse (in 2D) */
	if (draw2D) {
		G2Xpoint mouse = g2x_GetMousePosition();
		if (PointInWindow(mouse)) {
			Particle_followTarget(&dog, mouse, 0.5);
		}
	}

	/* Sheeps follow the dog */
	for (P = sheeps; P < sheeps + NB_SHEEPS; P++) {
		if (P->alive) {
			Particle_followTarget(P, dog.pos, 0.5);
		}
	}

	/* Hunter hunts */
	Particle_hunt(&hunter, sheeps, NB_SHEEPS, 0.65);

	/* Tracker follows the hunter */
	Particle_followTarget(&tracker, hunter.pos, 0.5);

	/* Camera follows the tracker */
	Particle_followTarget(&camera, tracker.pos, 0.5);

	/* Avoid collisions between sheeps */
	Particle_avoidCollisions(sheeps, NB_SHEEPS);

	/* Avoid obstacles */
	Particle_avoidAllObstacles(&dog, obstacles, NB_OBSTACLES);
	Particle_avoidAllObstacles(&hunter, obstacles, NB_OBSTACLES);
	for (P = sheeps; P < sheeps + NB_SHEEPS; P++) {
		if (P->alive) {
			Particle_avoidAllObstacles(P, obstacles, NB_OBSTACLES);
		}
	}
	if (!draw2D) {
		Particle_avoidAllObstacles(&camera, obstacles, NB_OBSTACLES);
	}

	/* Edges */
	for (P = sheeps; P < sheeps + NB_SHEEPS; P++) {
		if (P->alive) {
			P->edge(P);
		}
	}
	hunter.edge(&hunter);
	dog.edge(&dog);
	if (!draw2D) {
		camera.edge(&camera);
	}

	/* Moves */
	for (P = sheeps; P < sheeps + NB_SHEEPS; P++) {
		if (P->alive) {
			P->move(P);
		}
	}
	hunter.move(&hunter);
	dog.move(&dog);
	if (!draw2D) {
		updateCameraPosition();
		Dog3D_getOrientation();
		Dog3D_movePaws();
		Wolf3D_getOrientation();
		Wolf3D_movePaws();
	}
}

/*=============================================================================*/
/*= Rendering function                                                        =*/
/*=============================================================================*/
static void Draw(void) {
	glEnable(GL_MULTISAMPLE_ARB);
	if (draw2D) {
		draw_2D();
	} else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		draw_3D();
	}
}

/*=============================================================================*/
/*= Exit function                                                             =*/
/*=============================================================================*/
static void Exit(void) {
	releaseResources();
	fprintf(stderr, "Exited cleanly\n");
}

static bool PointInWindow(G2Xpoint M) {
	return M.x > 0.95 * xmin && M.x < 0.95 * xmax && M.y > 0.95 * ymin
			&& M.y < 0.95 * ymax;
}

static void initHunter(void) {
	Particle_createRandom(&hunter, xmin, xmax, ymin, ymax);
	hunter.color[0] = 0.0;
	hunter.color[1] = 1.0;
	hunter.color[2] = 0.0;
	hunter.radius = 4.0;
	hunter.speed = g2x_Vector_XY(1.3, 1.2);
	Particle_setDrawFunction(&hunter, Particle_drawSpot);
	Particle_setEdgeFunction(&hunter, Particle_rebound);
}

static void initCameraTracker(void) {
	Particle_createRandom(&camera, xmin, xmax, ymin, ymax);
	camera.speed = g2x_Vector_XY(1.2, 1.1);
	Particle_createRandom(&tracker, xmin, xmax, ymin, ymax);
	Particle_followTarget(&camera, tracker.pos, 0.5);
	tracker.speed = camera.speed;
	Particle_setEdgeFunction(&camera, Particle_rebound);
	Particle_setEdgeFunction(&tracker, Particle_rebound);
}

static void initObstacles(void) {
	Obstacle_createAllRandom(obstacles, NB_OBSTACLES, 20.);
}

static void initDog(void) {
	Particle_createRandom(&dog, xmin, xmax, ymin, ymax);
	dog.color[0] = 1.0;
	dog.color[1] = 0.0;
	dog.color[2] = 1.0;
	dog.radius = 1.4;
	dog.speed = g2x_Vector_XY(1.0, 1.0);
	Particle_setDrawFunction(&dog, Particle_drawSpot);
	Particle_setEdgeFunction(&dog, Particle_rebound);
}

static void initSheeps(void) {
	Particle *P;
	for (P = sheeps; P < sheeps + NB_SHEEPS; P++) {
		Particle_createRandom(P, xmin, xmax, ymin, ymax);
		P->color[0] = 1.0;
		P->color[1] = 1.0;
		P->color[2] = 1.0;
		P->radius = 1.0;
		P->speed = g2x_Vector_XY(0.8, 0.6);
		Particle_setDrawFunction(P, Particle_drawSpot);
		Particle_setEdgeFunction(P, Particle_rebound);
	}
}

static void updateCameraPosition(void) {
	tracker.move(&tracker);
	camera.move(&camera);
	createG3Xpoint(&camera.pos, posCamera);
	createG3Xpoint(&tracker.pos, posTracker);
	posCamera[Z_AXIS] = 1.5;
	posTracker[Z_AXIS] = 1.5;
}

static void draw_2D(void) {
	Obstacle *c;
	Particle *P;
	for (c = obstacles; c < obstacles + NB_OBSTACLES; c++) {
		g2x_FillCircle(c->center.x, c->center.y, c->radius, c->color);
	}
	for (P = sheeps; P < sheeps + NB_SHEEPS; P++) {
		if (P->alive) {
			P->draw(P);
		}
	}
	hunter.draw(&hunter);
	dog.draw(&dog);
}

static void draw_3D(void) {
	Obstacle *c;
	Particle *p;
	int i;

	drawGround();
	drawWolf(&hunter);
	drawDog(&dog);
	for (c = obstacles, i = 0; c < obstacles + NB_OBSTACLES; c++, i++) {
		if (i % 2) {
			drawTree(c);
		} else {
			drawRock(c);
		}
	}
	for (p = sheeps; p < sheeps + NB_SHEEPS; p++) {
		if (p->alive) {
			drawSheep(p);
		}
	}
}
