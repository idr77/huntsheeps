CC = gcc

INCLUDE_DIR = ./include
SRC_DIR = ./src

DEBUG_ECLIPSE = yes

ifeq ($(DEBUG_ECLIPSE), yes)
iGL_PATH=/usr/include
lGL_PATH=/usr/lib
PROCBIT=64b
G2X_PATH=/home/eric/Documents/Cours/CoursL3/S6/Geo3D/Libs/libg2x
G3X_PATH=/home/eric/Documents/Cours/CoursL3/S6/Geo3D/Libs/libg3x
incG3X=-I$(G3X_PATH)/include -I$(iGL_PATH)
libG3X=-lm -L$(lGL_PATH) -lGL -lGLU -lglut -L$(G3X_PATH) -lg3x.$(PROCBIT)
libG2X=-lm -L$(lGL_PATH) -lGL -lGLU -lglut -L$(G2X_PATH) -lg2x.$(PROCBIT)
incG2X=-I$(G2X_PATH)/include -I$(iGL_PATH)
endif

CFLAGS = -I$(INCLUDE_DIR) -W -Wall -Wextra -ansi -g $(incG2X) $(incG3X)
LDFLAGS = $(libG2X) $(libG3X)

SRC = $(wildcard $(SRC_DIR)/*.c)
EXEC = Anim

all: $(EXEC) clean

%: %.o Particle.o Obstacle.o GUI3D.o G3XTools.o CameraHandler.o Dog3D.o Wolf3D.o Sheep3D.o
	$(CC) -o $@ $^ $(LDFLAGS)

%.o : $(SRC_DIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -rf $(SRC_DIR)/*.o $(SRC_DIR)/*~ 

mrproper: clean
	rm -rf $(EXEC)
	