#ifndef _PARTICLE_H
#define _PARTICLE_H

#include <g2x.h>
#include "Obstacle.h"

/**
 * @brief Structure which represents a particle of the scene (it could be an animal, a camera ..)
 */
typedef struct _Particle {
	G2Xpoint pos; /* Position */
	G2Xvector speed; /* Speed */
	G2Xcolor color; /* Color */
	double radius; /* Radius */
	struct _Particle *next; /* Next particle (if list representation is used) */
	int xMin, xMax, yMin, yMax; /* Min and max coordinates of a particle. */
	bool alive; /* Dead or alive particle? */

	void (*draw)(struct _Particle*); /* 2-Dimensional Drawing function. */
	void (*edge)(struct _Particle*); /* Edge function (rebound, teleports ..) */
	void (*move)(struct _Particle*); /* Move function. */
} Particle;

typedef void (*ParticleFunc)(struct _Particle*); /* Function for a particle. */

/**
 * @brief Creates a particle with parameters.
 */
void Particle_create(Particle *p, int xMin, int xMax, int yMin, int yMax,
		G2Xpoint pos, G2Xvector speed, double radius, double color[]);

/**
 * @brief Creates randomly a particle.
 */
void Particle_createRandom(Particle *p, int xMin, int xMax, int yMin, int yMax);

/**
 * @brief Displays parameters of a particle (on shell). 
 */
void Particle_print(Particle *p);

/**
 * @brief Drawing function of a particle (spot). 
 */
void Particle_drawSpot(Particle *p);

/** 
 * @brief Drawing function of a particle (circle).
 */
void Particle_drawCircle(Particle *p);

/**
 * @brief Edge function of a particle (rebound). 
 */
void Particle_rebound(Particle *p);

/**
 * @brief Edge function of a particle (teleport).
 */
void Particle_teleport(Particle *p);

/**
 * @brief Moves the particle at its speed.
 */
void Particle_move(Particle *p);

/**
 * @brief Sets a 2D-drawing function at a particle.
 */
void Particle_setDrawFunction(Particle *p, ParticleFunc drawMethod);

/**
 * @brief Sets an edge function at a particle.
 */
void Particle_setEdgeFunction(Particle *p, ParticleFunc edgeMethod);

/**
 * @brief A particle follows another.
 */
void Particle_follow(Particle *follower, Particle *toFollow, double ratio);

/**
 * @brief A particle escapes another.
 */
void Particle_escape(Particle *runaway, Particle *toEscape, double ratio);

/**
 * @brief Checks whether the particle have a risk of collision with the obstacle.
 */
bool Particle_hasCollision(const Particle *p, const Obstacle *obstacle,
		G2Xvector *dir);

/**
 * @brief Deviates the particle around the obstacle.
 */
void Particle_deviate(Particle *p, const Obstacle *obstacle, G2Xvector *dir);

/**
 * @brief Deviates the particle around all obstacles of the scene.
 */
void Particle_avoidAllObstacles(Particle *p, Obstacle *obstacles,
		int nbObstacles);

/**
 * @brief The particle follows the target.
 */
void Particle_followTarget(Particle *p, G2Xpoint target, double alpha);

/**
 * @brief Avoids collision between a particle and another.
 */
void Particle_avoidCollision(Particle *p, Particle *other);

/**
 * @brief Avoids collisions between all particles of the scene.
 */
void Particle_avoidCollisions(Particle *particles, int nbParticles);

/**
 * @brief The particle hunts (and eats) other particles. 
 */
void Particle_hunt(Particle *hunter, Particle *hunteds, int nbHunteds,
		double ratio);

#endif
