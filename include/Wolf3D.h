#ifndef WOLF_3D_H
#define WOLF_3D_H

/**
 * @brief Sets the coordinates of the wolf.
 */
void Wolf3D_setParticleCoord(float posX, float posY, float speedX, float speedY,
		float radius);

/**
 * @brief Checks whether the the coordinates of the wolf are initialized.
 */
int Wolf3D_isSet(void);

/**
 * @brief Draws the wolf.
 */
void Wolf3D_draw(void);

/**
 * @brief Computes the orientation of the wolf.
 */
int Wolf3D_getOrientation(void) ;

/**
 * @brief Move paws of the wolf (Don't work).
 */
void Wolf3D_movePaws(void);

#endif
