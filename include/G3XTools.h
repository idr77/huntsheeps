#ifndef G3X_TOOLS_H
#define G3X_TOOLS_H 

#include <g3x.h>
#include <g2x.h>

#define X_AXIS 0
#define Y_AXIS 1
#define Z_AXIS 2

/**
 * @brief Operates a mod b.
 */
int modulo(int a, int n);

/**
 * @brief Converts an angle in radians at an in degrees.
 */
int rad2Deg(float rad);

/**
 * @brief Sets parameters of the light.
 */
void Light_setParameters(float ambiance, float diffuse, float specularity,
		float shine, float transparency);

/**
 * @brief Sets default parameters of the light.
 */
void Light_setDefault(void);
/**
 * @brief Contrast of the object.
 */
void Light_setAmbiance(float ambiance);

/**
 * @brief Luminosity of the object.
 */
void Light_setDiffusion(float diffusion);
/**
 * @brief Relief of the object.
 */
void Light_setSpecularity(float spec);

/**
 * @brief Light inside object.
 */
void Light_setShine(float shine);

/**
 * @brief Creates a G3Xpoint with a G2Xpoint.
 */
void createG3Xpoint(const G2Xpoint *pt, G3Xpoint p);

/**
 * @brief Saves the last transformations and returns at the point (0, 0, 0) of the scene.
 */
void pushMatrix();

/**
 * @brief Loads the last saved matrix of transformations.
 */
void popMatrix();

/**
 * @brief Makes the translation (x, y, z)
 */
void makeTranslation(float x, float y, float z);

/**
 * @brief Rotates around an axis (X_AXIS, Y_AXIS, Z_AXIS) (angle in degrees)
 */
void makeRotation(float angle, int axis);

/**
 * @brief Scales the object.
 */
void makeScale();

/**
 * @brief Renders a sphere.
 */
void renderSphere(G3Xcolor color, float size);

/**
 * @brief Renders a hemisphere.
 */
void renderHemisphere(G3Xcolor color, float radius);

/**
 * @brief Renders a cone.
 */
void renderCone(G3Xcolor color, float size, float height);

/**
 * @brief Renders a cylinder with different radius at both sides.
 */
void renderCylinder(G3Xcolor color, float radiusDown, float radiusUp,
		float height);

/**
 * @brief Renders a cylinder with the same radius at both sides.
 */
void renderPerfectCylinder(G3Xcolor color, float radius, float height);

/**
 * @brief Renders a cube.
 */
void renderCube(G3Xcolor color, float size);

/**
 * @brief Renders a rectangle.
 */
void renderRectangle(G3Xcolor color, float xmin, float xmax, float ymin,
		float ymax);

/**
 * @brief Releases resources allocated by createCylinder().
 */
void releaseResources(void);

#endif
