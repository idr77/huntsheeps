#ifndef CAMERA_HANDLER_H
#define CAMERA_HANDLER_H 
#include <g3x.h>

/**
 * @brief Sets the target of the camera.
 */
void Camera_setTarget(G3Xpoint t);

/**
 * @brief Camera zoom.
 */
void Camera_actionZoomIn(void);
void Camera_actionZoomOut(void);

/**
 * @brief Moves the camera to the left.
 */
void Camera_actionLeft(void);

/**
 * @brief Moves the camera to the right.
 */
void Camera_actionRight(void);

/**
 * @brief Raises the camera.
 */
void Camera_actionUp(void);

/**
 * @brief Lowers the camera.
 */
void Camera_actionDown(void);

#endif
