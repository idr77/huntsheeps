#ifndef DOG_3D_H
#define DOG_3D_H 

/**
 * @brief Sets the dog's coordinates.
 */
void Dog3D_setParticleCoord(float posX, float posY, float speedX, float speedY);

/**
 * @brief Checks whether the coordinates are initialized.
 */
int Dog3D_isSet(void);

/**
 * @brief Computes the dog's orientation.
 */
int Dog3D_getOrientation(void);

/**
 * @brief Moves paws of the dog (don't work).
 */
void Dog3D_movePaws(void);

/**
 * @brief Draws the dog.
 */
void Dog3D_draw(void);

#endif
