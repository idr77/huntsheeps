#ifndef SHEEP_3D_H
#define SHEEP_3D_H

#include "Particle.h"

/**
 * @brief Draws a sheep.
 */
void Sheep3D_draw(Particle *sheep);

#endif
