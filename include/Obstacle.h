#ifndef _OBSTACLE_H
#define _OBSTACLE_H

/* Pour les obstacles d'une autre forme qu'un cercle , on considèrera le cercle circonscrit à la forme géométrique comme obstacle. */
#include <g2x.h>

/**
 * @brief Structure used to represent an obstacle.
 */
typedef struct _Obstacle {
	G2Xpoint center;
	double radius;
	G2Xcolor color;
} Obstacle;

/**
 * @brief Creates an obstacle with parameters.
 */
void Obstacle_create(Obstacle *obstacle, float x, float y, float radius,
		G2Xcolor color);

/**
 * @brief Creates nbObstacles obstacles randomly.
 */
void Obstacle_createAllRandom(Obstacle *obstacles, int nbObstacles,
		double baseRadius);

#endif
