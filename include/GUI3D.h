#ifndef GUI_3D_H
#define GUI_3D_H

#include <g3x.h>
#include "Particle.h"
#include "Obstacle.h"

/**
 * @brief Min and max coordinates of the window (used for position of objects).
 */
extern double xmin, ymin, xmax, ymax;

/**
 * @brief Initializes the camera.
 */
void initCamera(G3Xpoint *camera, G3Xpoint *tracker);

/**
 * @brief Checks whether the camera is initialized or not.
 */
int isInitialized();

/**
 * @brief Draws the ground of the scene.
 */
void drawGround();

/**
 * @brief Displays a dog.
 */
void drawDog(Particle *dog);

/**
 * @brief Displays a sheep.
 */
void drawSheep(Particle *sheep);

/**
 * @brief Displays a wolf.
 */
void drawWolf(Particle *p);

/**
 * @brief Displays a rock.
 */
void drawRock(Obstacle *rock);

/**
 * @brief Displays a tree.
 */
void drawTree(Obstacle *tree);

#endif
